# Imgtiger
Imgtiger is an image viewer program for [iTerm2](https://iterm2.com). Imgtiger
was made because the reference implementation (imgcat) is slow and doesn't
support all parts of the protocol.

The inline image display protocol imgtiger is based on can be found at https://iterm2.com/documentation-images.html