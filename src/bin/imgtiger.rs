use std::fs;
use std::io::Result;
use std::path::{Path, PathBuf};
use structopt::StructOpt;

use imgtiger::*;

#[derive(StructOpt)]
/// Display an image in the terminal
struct Opt {
    #[structopt(short = "h", long = "height")]
    /// Sets the height of the image
    height: Option<Dimension>,

    #[structopt(short = "w", long = "width")]
    /// Sets the width of the image
    width: Option<Dimension>,

    #[structopt(short = "A", long = "no-preserve-aspect")]
    /// Do not preserve the image aspect ratio
    no_preserve_aspect: bool,

    #[structopt(short = "d", long = "download")]
    /// Download instead of displaying the file(s)
    is_download: bool,

    #[structopt(short = "n", long = "no-newline")]
    /// Do not add a newline after displaying an image
    no_newline: bool,

    // "required = true" because you have to specify at least one file.
    #[structopt(name = "file", required = true)]
    /// Files to display (- for stdin)
    files: Vec<PathBuf>,
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    let action = if opt.is_download {
        TransferAction::Download
    } else {
        TransferAction::Display {
            height: opt.height,
            width: opt.width,
            preserve_aspect_ratio: Some(opt.no_preserve_aspect),
        }
    };

    let stdout = std::io::stdout();
    let mut locked_stdout = stdout.lock();

    for file in opt.files {
        if file == Path::new("-") {
            FileTransfer::new(&action, Box::new(std::io::stdin()), None)
        } else {
            let file_name_option = file
                .file_name()
                .map(|o| o.to_str())
                .unwrap_or_else(|| file.to_str())
                .map(|s| s.to_owned());

            FileTransfer::new(&action, Box::new(fs::File::open(file)?), file_name_option)
        }
        .transfer(&mut locked_stdout)?;

        if !opt.is_download && !opt.no_newline {
            println!();
        }
    }

    Ok(())
}
